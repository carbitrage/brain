import { prepareData, getAllSignals, setSignalToNonSent } from './common/db';
import { Pool } from 'pg';
import { sendSignal } from './common/tg';

async function processSignals(pool: Pool): Promise<void> {
    return new Promise(async resolve => {
        const marketData: MarketData = await prepareData(pool);
        const signals: Signal[] = await getAllSignals(pool);
        await checkForAlarms(pool, marketData, signals);
        resolve();
    });
}

async function checkForAlarms(pool: Pool, marketData: MarketData, signals: Signal[]): Promise<void> {
    return new Promise<void>(resolve => {
        const testUID: string | undefined = process.env.TEST_UID;
        for (const signal of signals) {
            if (signal.uid.toString() !== testUID) {
                const pair: MarketAndPrice[] = marketData[signal.pair];
                const prices: number[] = pair.map(element => { return element.price }).sort((a, b) => { return a - b });
                const calculatedDifference: number = prices[prices.length - 1] - prices[0];
                if (calculatedDifference >= signal.difference) {
                    if (!signal.sent) {
                        sendSignal(pool, signal, calculatedDifference);
                    }
                } else {
                    if (signal.sent) {
                        setSignalToNonSent(pool, signal.id);
                    }
                }
            }
        }
        resolve();
    });
}

export { processSignals };